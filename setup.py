from setuptools import setup

setup(
    name='cookiecutter-python-conda-deep-learning',
    packages=[],
    version='0.1.0',
    description=
    'Cookiecutter template for reproducible research in deep learning using Python conda package ',
    author='Kai Huang',
    author_email='khuang5@mdanderson.org',
    url='https://gitlab.com/AmarilloChi/cookiecutter-deep-learning',
    keywords=[
        'cookiecutter', 'template', 'package', 'conda', 'python',
        'deep learning', 'reproducibility'
    ],
    classifiers=[
        'Development Status :: 1 - Beta',
        'Environment :: Console',
        'Intended Audience :: Developers',
        'Natural Language :: English',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: Implementation :: CPython',
        'Topic :: Software Development',
    ],
)
