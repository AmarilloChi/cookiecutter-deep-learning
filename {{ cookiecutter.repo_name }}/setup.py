#!/usr/bin/env python
from setuptools import find_packages, setup
import versioneer

{%- set license_classifiers = {
    'MIT license': 'License :: OSI Approved :: MIT License',
    'BSD license': 'License :: OSI Approved :: BSD License',
    'ISC license': 'License :: OSI Approved :: ISC License (ISCL)',
    'Apache Software License 2.0': 'License :: OSI Approved :: Apache Software License',
    'GNU General Public License v3': 'License :: OSI Approved :: GNU General Public License v3 (GPLv3)'
} %}

with open("envs/envs_1.txt", "r") as fh:
    requirements = [line.strip() for line in fh]

setup(
    name='{{ cookiecutter.repo_name }}',
    packages=find_packages(include=['{{ cookiecutter.repo_name }}', '{{ cookiecutter.repo_name }}.*']),
    version=versioneer.get_version(),
    cmdclass=versioneer.get_cmdclass(),
    description='{{ cookiecutter.description }}',
    author='{{ cookiecutter.full_name }}',
    author_email='{{ cookiecutter.email }}',
{%- if cookiecutter.license in license_classifiers %}
    license="{{ cookiecutter.license }}",
{%- endif %}
    url = '{{ cookiecutter.url }}',
    zip_safe=False,
    install_requires=requirements
)
