#!/usr/bin/env python
import os
from pathlib import Path
from subprocess import run, CalledProcessError
import shlex
import time

PROJECT_DIRECTORY = os.path.realpath(os.path.curdir)
pkg_name = '{{ cookiecutter.repo_name }}'
repo = '{{ cookiecutter.url }}'


def remove_file(filepath):
    os.remove(os.path.join(PROJECT_DIRECTORY, filepath))


def init_repo():
    """Initialize a git repository for this project."""
    print(f"Initializing development environment for {pkg_name}")
    try:
        git_init = run('git init .'.split(), check=True)
        print('Initialized git repository')
        if repo:
            git_add_remote = run(f'git remote add origin {repo}'.split(),
                                 check=True)
            print(f'Found url, set origin: {repo}')
        git_add = run('git add -A'.split(), check=True)
        git_commit = run(
            shlex.split(f'git commit -m "first commit of {pkg_name} "'),
            check=True)
        git_tag = run(shlex.split('git tag -a -m "first tag" 0.0.1'),
                      check=True)
        print('First commit.')
    except CalledProcessError as e:
        print(e)


def install_versioneer():
    """install versioneer package for this project, and commit to git."""
    print(f"Install versioneer package for {pkg_name}")
    try:
        # conda_versioneer = run(
        #     'conda install -c conda-forge versioneer'.split(), check=True)
        run_versioneer = run('versioneer install'.split(),
                             check=True,
                             shell=True)
        print('Installed versioneer')
        # wait for a min. The install versioneer process might not finish
        # before git starts editing
        print("wait for 60 seconds for the versioneer to finish installing")
        time.sleep(60)
        # pipenv_install_dev = run('pipenv run pip install -e .'.split(),
        #                          check=True)
        # print('Installed package in development mode.')
        git_add_after = run('git add -A'.split(), check=True)
        git_commit_after = run(
            shlex.split('git commit -m "added versioneer support."'),
            check=True)
        print('second commit.')
        print('All set!')
    except CalledProcessError as e:
        print(e)


if __name__ == '__main__':
    if 'no' in '{{ cookiecutter.command_line_interface|lower }}':
        remove_file('cli.py')

    if '{{ cookiecutter.license }}' == 'Proprietary':
        remove_file('LICENSE')

    init_repo()
    install_versioneer()