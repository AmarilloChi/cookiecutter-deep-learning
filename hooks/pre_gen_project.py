#!/usr/bin/env python
import os
import sys
import re
from subprocess import run, CalledProcessError

PROJECT_DIRECTORY = os.path.realpath(os.path.curdir)
MIN_PYTHON_VER = (3, 6)
if sys.version_info < MIN_PYTHON_VER:
    sys.exit("Python 3.6 or later is required!")


def check_prerequisites():
    try:
        run("git --version".split(), check=True)
    except CalledProcessError as e:
        print("git is required!")
        raise FileNotFoundError
    try:
        run("conda --version".split(), check=True)
    except CalledProcessError as e:
        print("conda is required!")
        # res = input("Attempt to install pipenv? [Y/n]: ")
        # if not res or res.casefold() == "y":
        #     try:
        #         pipenv_install = run("pip install --user pipenv".split(),
        #                              check=True)
        #     except CalledProcessError:
        #         raise FileNotFoundError
        # else:
        #     raise FileNotFoundError


def check_repo_name():
    MODULE_REGEX = r'^[_a-zA-Z][_a-zA-Z0-9]+$'
    module_name = '{{ cookiecutter.repo_name}}'
    if not re.match(MODULE_REGEX, module_name):
        print(
            'ERROR: The project repo_name (%s) is not a valid Python repo name. Please do not use a - and use _ instead'
            % module_name)

        #Exit to cancel project
        sys.exit(1)


if __name__ == '__main__':
    # check_repo_name()
    check_prerequisites()
